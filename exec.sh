#!/bin/bash
# For scale containers use: docker-compose scale db|web=NUMBER
# For stop all containers use the follow command inside compose directory: docker-compose stop
# Author: Guilherme Martins
# Grow this infrastructure every single day :)

USE_MESSAGE="
Uso: $(basename "$0") [OPÇÕES]

OPTIONS:
	-h, --help 	Show help menu
	-w, --web  	Up Web server
	-d, --db 	Up Postgres
	-a, --all	Up all stack
"

function upWeb(){
	docker-compose run web django-admin.py startproject webserver .
	
	echo "Don't forget, adjust database name in settings.py inside webserver directory"

}

function upDB(){
	#not ready yet
	echo "option not ready yet, sorry :("
}

if [[ -z $1 ]]; then
    echo "$USE_MESSAGE"
fi

while [[ -n "$1" ]]; do
	case "$1" in
		-h | --help) 	echo "$MENSAGEM_USO" && exit 0 ;;
		-a | --all)	upWeb && installDB && docker-compose up -d && exit 0 ;; 
		-w | --web)	upWeb && exit 0;;
		-d | --db)	upDB && exit 0;;
		*) echo "Invalid option, use -h or --help for help" && exit 1 ;;
	esac
	shift
done
